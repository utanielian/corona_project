import numpy as np
import csv
import sys
import requests
import os
import matplotlib.pyplot as plt
from scipy.stats import rankdata

from utils import plot_analysis_country, does_the_x_previous_days_can_predict_today, get_data_for_a_country, get_all_dates, get_confinement, get_next_x_days, \
    get_lower_thresholds, movingaverage


def from_string_to_int(stringg):
    if (stringg==""):
        return 0
    else:
        return int(stringg)


def add_item(item, value, dic):
    if item == "United States" or item=="United States of America":
        item = "USA"
    if item not in dic:
        if value > 0 :
            dic[item] = value
        else:
            dic[item] = len(dic)
    return dic[item]


def download_data():
    if not os.path.exists("data/"):
        os.makedirs('data/')
    url = 'https://covid.ourworldindata.org/data/ecdc/full_data.csv'
    #url = 'https://covid.ourworldindata.org/data/full_data.csv'
    #url = https://www.ecdc.europa.eu/sites/default/files/documents/COVID-19-geographic-disbtribution-worldwide.xlsx
    r = requests.get(url, allow_redirects=True)
    open('data/full_data.csv', 'wb').write(r.content)

    url2 = 'https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7'
    r = requests.get(url2, allow_redirects=True)
    open('data/france_hospital.csv', 'wb').write(r.content)


def load_data(all_dates):
    download_data()
    data = list()
    dic_locs = dict()
    dic_countries_pop = dict()
    with open('data/covid19_pop.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count>0 and row[9] != "":
                add_item(row[6].replace("_", " "), int(row[9]), dic_countries_pop)
            line_count += 1

    with open('data/full_data.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count==0:
                a=1#print(row)
            if line_count>0:
                index_country = add_item(row[1], 0, dic_locs)
                row2 = [np.where(row[0]==all_dates)[0][0], index_country, from_string_to_int(row[2]), from_string_to_int(row[3]), \
                    from_string_to_int(row[4]), from_string_to_int(row[5])]
                data.append(row2)
            else:
                col_names = row
            line_count += 1

    with open('data/france_hospital.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        days, deaths, rea = list(), list(), list()
        for row in csv_reader:
            if line_count==0:
                a=1#print(row)
            else:
                row_aux = row[0].split(";")
                days.append(np.where(row_aux[2][1:-1]==all_dates)[0][0])
                deaths.append(int(row_aux[6]))
                rea.append(int(row_aux[4]))
            line_count += 1
        deaths_france_hos = np.column_stack((days, rea, deaths))
        dico_deaths_france = dict()
        for elem in deaths_france_hos:
            if elem[0] not in dico_deaths_france:
                dico_deaths_france[elem[0]] = [int(int(elem[1])/2), int(int(elem[2])/2)]
            else:
                dico_deaths_france[elem[0]][0] += int(int(elem[1])/2)
                dico_deaths_france[elem[0]][1] += int(int(elem[2])/2)
    
    previous_value = [0,0]
    for date, value in dico_deaths_france.items():
        data.append([date, add_item("France_hosp", 0, dic_locs), value[0]-previous_value[0], value[1]-previous_value[1], value[0], value[1]])
        previous_value = value

    dic_countries_pop["France_hosp"] = dic_countries_pop["France"]
    data = np.array(data)
    return col_names, data, dic_locs, dic_countries_pop