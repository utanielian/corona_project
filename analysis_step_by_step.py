import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import polynomial, extend, get_scenarios, sigmoid, get_polynomial_trend, get_total_numbers, exponential, exponential_with_b_inside, \
    linear, get_stop_day_plot, exponential_single, exponential_just_a
from scenarios_utils import first_scenario_predictions

dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}

def get_analysis_step_by_step_country(country, x_n, y_n, x_t, y_t, num_days, confinement, feature, all_dates, step, cumulative):
    if cumulative:
        x, y = 1 + x_t - np.amin(x_t), y_t
    else:
        x, y = 1 + x_n - np.amin(x_n), y_n
    
    begs, stops, models_exp0 = list(), list(), list()
    last_exp, last_pol, stop, i = 0, 0, 0, 0
    while stop<len(x)-1:
        beg, stop, i = i*step, min((i+1)*step+1, len(x)-1), i + 1
        if (len(x)-1-stop<3):
            stop = len(x)-1
        x_step, y_step = x[beg:stop]-x[beg]+1, y[beg:stop]/y[beg]
        popt_exp, _ = curve_fit(exponential_just_a, x_step, y_step, method="dogbox", maxfev=5000)
        #popt_pol, _ = curve_fit(exponential, np.log(x_step), y_step, method="dogbox", maxfev=5000)
        models_exp0.append(popt_exp[0])
    return models_exp0
        

def plot_analysis_step_by_step_country(country, x_n, y_n, x_t, y_t, num_days, confinement, feature, all_dates, step, cumulative, plot, pred):
    #TECHNICAL DETAILS
    subfolder = "analysis" + "/" + country + "/"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    if feature=="tc":
        feature_word, feature_final = "cas", "Nombre de cas" #"Number of cases"
    else:
        feature_word, feature_final = "décès", "Nombre de morts" #"Number of deaths"

    if plot:
        plt.clf()
        f = plt.figure(figsize=(16,16))
        ax = f.add_subplot(111)
        ax.plot(all_dates[x_t], y_t, color="k", marker=".", linestyle = 'None', markersize=18, label="Cas observés jusqu'à aujourd'hui")
    
    if confinement>0 and plot:
        confinement_date = np.where(confinement==x_t)[0][0]
        ax.axvline(x=all_dates[x_t[confinement_date]], color="k", linestyle="solid", linewidth=7, alpha=0.9, \
            label="Confinement le " + str(all_dates[x_t[confinement_date]]) + " (" + feature_final.lower() + ": " + str(y_t[confinement_date]) + ").")     
    
    if cumulative:
        x, y = 1 + x_t - np.amin(x_t), y_t
    else:
        x, y = 1 + x_n - np.amin(x_n), y_n
    begs, stops, models_exp0, models_exp1, models_pol0, models_pol1 = list(), list(), list(), list(), list(), list()
    last_exp, last_pol, stop, i = 1, 1, 0, 0
    while stop<len(x_t)-1:
        beg, stop, i = i*step, min((i+1)*step+1, len(x_t)-1), i + 1
        if (len(x_t)-1-stop<3):
            stop = len(x)-1
        if cumulative:
            x_step, y_step = x[beg:stop]-x[beg]+1, y[beg:stop]/y[beg]
            popt_exp, _ = curve_fit(exponential_just_a, x_step, y_step, method="dogbox", maxfev=5000)
            popt_pol, _ = curve_fit(exponential, np.log(x_step), y_step, method="dogbox", maxfev=5000)
            model_exp = exponential_just_a(x_step, popt_exp[0], popt_exp[1], popt_exp[2])*y[beg]
            model_pol = exponential(np.log(x_step), popt_pol[0], popt_pol[1], popt_pol[2])*y[beg]
            last_exp, last_pol = model_exp[-1], model_pol[-1]
        else:
            x_step, y_step = x[beg:stop], y[beg:stop]
            popt_exp, _ = curve_fit(exponential, x_step, y_step, method="dogbox", maxfev=5000)
            popt_pol, _ = curve_fit(exponential, np.log(x_step), y_step, method="dogbox", maxfev=5000)
            model_exp = np.cumsum(exponential(x_step, popt_exp[0], popt_exp[1], popt_exp[2])) + last_exp - exponential(beg, popt_exp[0], popt_exp[1], popt_exp[2])
            model_pol = np.cumsum(exponential(np.log(x_step), popt_pol[0], popt_pol[1], popt_pol[2])) + last_pol - \
                exponential(np.log(beg), popt_pol[0], popt_pol[1], popt_pol[2])
            last_exp, last_pol = model_exp[-1], model_pol[-1]
        models_exp0.append(popt_exp[0])

        if plot:
            ax.plot(x[beg:stop], model_exp, color="r", linestyle = '--', alpha = 0.5, linewidth=7)
                #linestyle = '--', alpha = 0.5, linewidth=7, label="Model exponential pré confinement, coeff = " + str(round(popt_exp[0], 2)))
            #ax.plot(x[beg:stop], model_pol, color="b", \
            #    linestyle = '--', alpha = 0.5, linewidth=7, label="Model polynomial pré confinement, coeff = " + str(round(popt_pol[0], 2)))
    
    if plot and pred and cumulative :
        num_steps_future = 20
        x_future = np.arange(num_steps_future*step)+x[stop]
        popt_prev0, _ = curve_fit(exponential_single, np.arange(len(models_exp0)), models_exp0, method="dogbox", maxfev=5000)
        model_exp0_prev = exponential_single(np.arange(len(models_exp0), len(models_exp0) + num_steps_future), popt_prev0[0], popt_prev0[1], popt_prev0[2])
        if model_exp0_prev[-1]<0.1:
            last_pred = y[-1]
            for i in range(num_steps_future):
                beg, stop = i*step, (i+1)*step
                model_prev = exponential_just_a(np.arange(1, 1+step), model_exp0_prev[i])*last_pred
                last_pred = model_prev[-1]
                ax.plot(x_future[beg:stop], model_prev, color="r", linestyle = 'dotted', alpha = 0.5, linewidth=7)
    
    if plot:
        plt.xlabel("Time in days", fontsize=25)
        plt.ylabel(feature_final, fontsize=25)
        xticks = x[np.arange(0, len(x), step=6)]
        plt.xticks(xticks)
        plt.title(country.upper(), fontsize=30)
        lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(0.4, -0.35), fontsize=23, loc="center", prop={'size':25})
        plt.tick_params(axis="both", labelsize=25)
        ax.tick_params(axis='x', rotation=45)
        plt.savefig(subfolder + "/" + country + "_" + feature_final + "_model_step_by_step", bbox_extra_artists=(lgd,), bbox_inches='tight')
        plt.close()