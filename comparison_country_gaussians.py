import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import normal, extend


def fitting_normals_for_daily_cases(x, y):
    inflexion_point = np.argmax(y)
    popt_normal_1, _ = curve_fit(normal, np.log(x[:inflexion_point]), y[:inflexion_point])
    popt_normal_2, _ = curve_fit(normal, np.log(x[inflexion_point:]), y[inflexion_point:])
    return 1/popt_normal_1[0], 1/popt_normal_2[0]


def plot_predictions_country_normal_law(country):
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    ax.plot(x, y, color="k", marker=".", linestyle = 'None', markersize=15, label="Cas observés jusqu'à aujourd'hui")


def plot_comparison_all_countries(data, list_countries, feature, mode_data, trend_boolean, all_dates, dic_locs, step=0.0025, bc=1.25, wc=1.75, gmax=52, trend=5):
    today_minust_two_days = date.today() - timedelta(days=2)
    today, yesterday, daybefore = date.today(), date.today()-timedelta(days=1), date.today()-timedelta(days=2)
    subfolder = "comparisons_gaussians" + "/" 
    subfolder = "comparisons_gaussians" + "/" + "trend"  + "/" if trend_boolean else "comparisons" + "/" + "no_trend" + "/"
    feature_word = "cases" if feature=="tc" else "deaths"

    if mode_data=="confinement":
        subfolder += "confinement"
        title = "Number of " +feature_word+" starting from confinement. Model as of " + today_minust_two_days.strftime("%d/%m/%Y")+ "."
    else:
        subfolder += "no_confinement"
        title = "Number of " +feature_word+". Model as of " + today_minust_two_days.strftime("%d/%m/%Y") + "."
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    print("")
    plt.clf()
    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False, figsize=(100,60), dpi=50)
    plt.subplots_adjust(hspace=0.4, top=0.85, right=0.50)
    cmap = plt.cm.get_cmap('jet')
    colors = ["red", "green", "orange", "blue", "cyan"]

    extra_artists = list()
    max_total_numbers = list()

    for i, country in enumerate(list_countries):
        c = colors[i]
        _, y_n, x, y = get_data_country_aux(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        
        axs[1].plot(np.arange(len(y_n)), y_n, linewidth=7, linestyle=None, marker="o", markersize=8, c=c)
        axs[2].plot(np.arange(len(y)), y, linewidth=7, linestyle=None, marker="o", markersize=8, c=c, label=country.upper())

        if len(y_ratio)<5 or np.argmax(movingaverage(y_ratio, 4))>len(movingaverage(y_ratio, 4))-2:
            print(country + " is still too soon to call ...")
            extra_artists.append(plt.text(0.97, -1.80+0.15*i, "Still too soon to call for " + country.upper(), size=70, \
                horizontalalignment='left', transform = axs[1].transAxes))
            continue
        if len(y_ratio)>50 or np.mean(y_n[-3:]<2):
            print(country + " has beatten the coronavirus...")
            continue

        popt_exp, popt_exp_rel, beg_date, stop_date = \
            get_relative_and_leastsquare_exponential_factor(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        print(country, popt_exp, popt_exp_rel)

        popt_exp[0], low_exp0, up_exp0 = get_lower_and_upper_bounds(popt_exp, popt_china, popt_italy, beg_date, stop_date, step, len(y_ratio))
        print(country, popt_exp[0], low_exp0, up_exp0)
        model_prev, low_prev, up_prev, model_prev_total, low_prev_total, up_prev_total, model_prev_diff, low_prev_diff, up_prev_diff = \
            get_all_pred_models(popt_exp, gmax, y[stop_date], stop_date, step, low_exp0, up_exp0)

        popt_exp_rel[0], low_exp0, up_exp0 = get_lower_and_upper_bounds(popt_exp_rel, popt_china_rel, popt_italy_rel, beg_date, stop_date, step, len(y_ratio))
        print(country, popt_exp_rel[0], low_exp0, up_exp0)
        _, _, _, model_prev_total_rel, low_prev_total_rel, up_prev_total_rel, model_prev_diff_rel, low_prev_diff_rel, up_prev_diff_rel = \
            get_all_pred_models(popt_exp_rel, gmax, y[stop_date], stop_date, step, low_exp0, up_exp0)
        low_prev_total, low_prev_diff, up_prev_total, up_prev_diff = get_the_smallest_minust_five_pct(low_prev_total, low_prev_total_rel), \
            get_the_smallest_minust_five_pct(low_prev_diff, low_prev_diff_rel), get_the_biggest_minust_five_pct(up_prev_total, up_prev_total_rel), \
            get_the_biggest_minust_five_pct(up_prev_diff, up_prev_diff_rel)

        max_total_numbers.append(low_prev_total[-1])
        
        #PLOTTING THE MODELS
        axs[0].plot(np.arange(gmax), model_prev, linewidth=9, linestyle="dashed", c=c)
        axs[1].plot(np.arange(gmax)[stop_date:-1], low_prev_diff, linewidth=9, c=c, linestyle="dotted")
        axs[1].plot(np.arange(gmax)[stop_date:-1], up_prev_diff, linewidth=9, c=c, linestyle="dotted")
        axs[2].plot(np.arange(gmax)[stop_date:], low_prev_total, linewidth=9, c=c, linestyle="dotted")
        axs[2].plot(np.arange(gmax)[stop_date:], up_prev_total, linewidth=9, c=c, linestyle="dotted")
        extra_artists.append(plt.text(1.05, 2.20-0.12*(i+1), country.upper()[:3] + " from "+ str(round(up_exp0, 3)) +" to "+ str(round(low_exp0, 3)) , size=69, horizontalalignment='left', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.05, 0.95-0.12*(i+1), country.upper()[:3], size=69, horizontalalignment='left', transform = axs[1].transAxes)) 
        extra_artists.append(plt.text(1.25, 0.95-0.12*(i+1), str(int(y_n[-2]))+ "--" + str(int((low_prev_diff[0]))) + ":" + str(int((up_prev_diff[0]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.50, 0.95-0.12*(i+1), str(int(y_n[-1]))+ "--" + str(int((low_prev_diff[1]))) + ":" + str(int((up_prev_diff[1]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.75, 0.95-0.12*(i+1), str(int((low_prev_diff[2]))) + ":" + str(int((up_prev_diff[2]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        
        extra_artists.append(plt.text(1.05, -0.75-0.12*(i+1), country.upper()[:3], size=69, horizontalalignment='left', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.20, -0.75-0.12*(i+1), str(int(y[-1])), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.42, -0.75-0.12*(i+1), str(int(low_prev_total[7]/100)*100) + " to " + str(int(up_prev_total[7]/100)*100), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.70, -0.75-0.12*(i+1), str(int(low_prev_total[-1]/100)*100) + " to " + str(int(up_prev_total[-1]/100)*100), size=69, horizontalalignment='center', transform = axs[1].transAxes))

        if np.amax(y_n)<np.amax(model_prev_diff):
            maxx, peak_day = int(np.amax(model_prev_diff)), all_dates[x[stop_date]+np.argmax(model_prev_diff)]
        else:
            maxx, peak_day = int(np.amax(y_n)), all_dates[x[np.argmax(y_n)]]
        plt.text(1.05, 0.30-0.12*(i+1), country.upper()[:3] + "  "+ peak_day + " with " + str(maxx) + " " + feature_word, size=69, \
            horizontalalignment='left', transform = axs[1].transAxes)

    plt.xlabel("Days after confinement", fontsize=85)
    plt.text(0.05, 2.50, "1) Growth w.r.t. the previous day", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.05, 1.10, "2) Evolution of the number of daily " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.05, -0.30, "3) Evolution of the total number of " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.35, 2., r"$S_t$ = " + "number of total " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes)
    plt.text(0.35, 1.8, r"$S_{t+1}/S_t = e^{-\alpha t + \beta} + 1$.", size=85, horizontalalignment='left', transform = axs[1].transAxes)
    plt.text(1.05, 2.20, "Country's "+ r"$\alpha$"+ " parameter", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red")
    extra_artists.append(plt.text(1.05, 1.15, "Daily number of " + feature_word, horizontalalignment='left', transform = axs[1].transAxes, color="red", size=85))
    #extra_artists.append(plt.text(1.05, 1.18, "Dates",horizontalalignment='left', transform = axs[1].transAxes, color="red", size=85))
    extra_artists.append(plt.text(1.25, 1.05, daybefore.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.50, 1.05, yesterday.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.75, 1.05, today.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.25, 0.95, "data --- past ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.50, 0.95, "data --- past ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.75, 0.95, "future ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.05, 0.30, "Estimation of the peak day", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    extra_artists.append(plt.text(1.05, -0.65, "Total number of "+feature_word, size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    extra_artists.append(plt.text(1.20, -0.75, "Today " + today.strftime("%d/%m"), size=69, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.42, -0.75, "in 5 days", size=69, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.70, -0.75, "in 50 days", size=69, horizontalalignment='center', transform = axs[1].transAxes))
    #extra_artists.append(plt.text(1.05, -0.40, "Estimations +5 days", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    #extra_artists.append(plt.text(1.05, -1.10, "Estimations +50 days", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    if "USA" in list_countries:
        extra_artists.append(plt.text(0.97, -1.80, "The confinement date used for USA is the 23/03/2020.", size=70, horizontalalignment='left', transform = axs[1].transAxes))
    
    #TECHNICAL DETAILS FOR THE PLOT
    axs[0].set_ylabel(r"$S_{t+1}/S_t$", fontsize=85)
    axs[1].set_ylabel(r"$S_{t+1}-S_t$", fontsize=85)
    axs[2].set_ylabel(r"$S_t$", fontsize=85)
    plt.xticks(np.arange(0, gmax, step=4))
    extra_artists.append(plt.suptitle(title, x=0.40, y=0.95, fontsize=100))
    extra_artists.append(plt.legend(frameon=False, ncol=len(list_countries), borderaxespad=0., bbox_to_anchor=(0.5, -0.40), \
        fontsize=23, loc="center", labelspacing=0.9, prop={'size':80}))
    axs[0].tick_params(axis="both", labelsize=80)
    axs[0].tick_params(axis='x', rotation=45)
    axs[1].tick_params(axis="both", labelsize=80)
    axs[1].tick_params(axis='x', rotation=45)
    axs[2].tick_params(axis="both", labelsize=80)
    axs[2].tick_params(axis='x', rotation=45)
    #axs[2].set_ylim(bottom=min(max_total_numbers)/2)
    axs[1].set_yscale("log")
    axs[2].set_yscale("log")
    
    fig.savefig(subfolder + "/" + "prediction_countries_"+feature_word+"_"+"_".join(list_countries), bbox_extra_artists=extra_artists, bbox_inches='tight')
    fig.savefig(subfolder + "/" + "prediction_countries_"+feature_word+"_"+"_".join(list_countries)+".pdf", bbox_extra_artists=extra_artists, bbox_inches='tight')
    #extent = axs[2].get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    #fig.savefig(subfolder + "/" + feature, bbox_inches=extent.expanded(1.1, 2.))
    plt.close()