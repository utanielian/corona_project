import numpy as np
import csv
import sys
import requests
import matplotlib.pyplot as plt
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import plot_analysis_country, does_the_x_previous_days_can_predict_today, get_data_for_a_country, get_all_dates, get_confinement, get_next_x_days, \
    get_lower_thresholds, movingaverage, get_data_country_aux, lognormal, logcauchy, normal, log_normal, loglog_normal
from load_data import load_data
from analysis_confinement import plot_impact_confinement_country
from analysis_step_by_step import plot_analysis_step_by_step_country, get_analysis_step_by_step_country
from predictions_based_on_cumulative_data import plot_predictions_country
from comparison_country import plot_comparison_all_countries, plot_comparison_growth_all_countries, plot_comparison_lognumber_all_countries, \
    plot_comparison_ratios_all_countries, plot_comparison_daily_all_countries


def analysis_country(country, data, next_x_days, confinement, l1, l2, all_dates):
    step = 5
    x_n, y_n, x_t, y_t = get_data_country_aux(country, data, "nc", "tc", l=l1)
    x_n, y_n, x_t, y_t = x_n[np.where(x_n==confinement)[0][0]:], y_n[np.where(x_n==confinement)[0][0]:], \
        x_t[np.where(x_n==confinement)[0][0]:], y_t[np.where(x_n==confinement)[0][0]:]
    plot_analysis_country(x_n, y_n, country, feature="nc", dic_locs=dic_locs)
    does_the_x_previous_days_can_predict_today(x_n, y_n, number_previous_days=25)
    plot_analysis_country(x_t, y_t, country, feature="tc", dic_locs=dic_locs)
    #plot_impact_confinement_country(country, x_n, y_n, x_t, y_t, num_days= 30, confinement=confinement, feature="tc", all_dates=all_dates, l_threshold=l1)
    plot_analysis_step_by_step_country(country, x_n, y_n, x_t, y_t, num_days= 30, confinement=confinement, feature="tc", \
        all_dates=all_dates, step=step, cumulative=True, plot=True, pred=True)
    
    x_n, y_n, x_t, y_t = get_data_country_aux(country, data, "nd", "td", l=l2)
    x_n, y_n, x_t, y_t = x_n[np.where(x_n==confinement)[0][0]:], y_n[np.where(x_n==confinement)[0][0]:], \
        x_t[np.where(x_n==confinement)[0][0]:], y_t[np.where(x_n==confinement)[0][0]:]
    plot_analysis_country(x_n, y_n, country, feature="nd", dic_locs=dic_locs)
    does_the_x_previous_days_can_predict_today(x_n, y_n, number_previous_days=25)
    plot_analysis_country(x_t, y_t, country, feature="td", dic_locs=dic_locs)
    #plot_impact_confinement_country(country, x_n, y_n, x_t, y_t, num_days= 30, confinement=confinement, feature="td", all_dates=all_dates, l_threshold=l2)
    plot_analysis_step_by_step_country(country, x_n, y_n, x_t, y_t, num_days= 30, confinement=confinement, feature="td", \
        all_dates=all_dates, step=step, cumulative=True, plot=True, pred=True)


def analysis_world_without_china(country, data):
    x_n_world, y_n_world, x_t_world, y_t_world = get_data_country_aux(dic_locs["World"], data, "nc", "tc", l=l1)
    x_n_china, y_n_china, x_t_china, y_t_china = get_data_country_aux(dic_locs["China"], data, "nc", "tc", l=l1)
    plot_analysis_country(x_china, y_world[-len(x_china):]-y_china, country, feature=feature, dic_locs=dic_locs)

def predictions_country(country, data, next_x_days, confinement, l1, l2, all_dates):
    x_n, y_n, x_t, y_t = get_data_country_aux(country, data, "nc", "tc", l=l1)
    plot_predictions_country(country, x_n, y_n, x_t, y_t, removing_last_x_days=0, next_x_days=next_x_days, feature = "tc", confinement=confinement, all_dates=all_dates)
    x_n, y_n, x_t, y_t = get_data_country_aux(country, data, "nd", "td", l=l2)
    plot_predictions_country(country, x_n, y_n, x_t, y_t, removing_last_x_days=0, next_x_days=next_x_days, feature = "td", confinement=confinement, all_dates=all_dates)


def comparison_all_countries(data, list_countries, all_dates, dic_locs, dic_countries_pop):
    def comparison_aux(data, feature, mode_data, trend_boolean):
        plot_comparison_daily_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, normed=False, fit_function=lognormal)
        plot_comparison_daily_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, normed=True, fit_function=lognormal)
        plot_comparison_daily_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, normed=False, fit_function=logcauchy)
        plot_comparison_daily_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, normed=True, fit_function=logcauchy)
        plot_comparison_lognumber_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, per_capita_boolean=True)
        plot_comparison_lognumber_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, per_capita_boolean=False)
        plot_comparison_ratios_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs)
        plot_comparison_all_countries(data, list_countries, feature, mode_data, trend_boolean, all_dates, dic_locs)

    #here: everything starts at the confinement date of each country
    print("Since confinement")
    comparison_aux(data, "tc", mode_data="confinement", trend_boolean=True)
    comparison_aux(data, "td", mode_data="confinement", trend_boolean=True)
    comparison_aux(data, "tc", mode_data="confinement", trend_boolean=False)
    comparison_aux(data, "td", mode_data="confinement", trend_boolean=False)
    
    #here: everything starts at at precise threshold
    #print("")
    #print("")
    #print("Since threshold")
    #comparison_aux(data, "tc", mode_data="threshold", trend_boolean=True)
    #comparison_aux(data, "td", mode_data="threshold", trend_boolean=True)
        

all_dates, all_dates_year = get_all_dates(2019, 12, 30, 250)
#LOAD DATA
col_names, data, dic_locs, dic_countries_pop = load_data(all_dates_year)
data[:,0] = rankdata(data[:,0], method='dense')
dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}
dico_from_dates_to_ints = {}

#HERE YOU CAN PLOT DATA TO SEE WHAT WE HAVE
#print(data)

#DEFINE LIST COUNTRIES
#list_countries = ["Italy", "Spain", "France", "USA"]#, "Chile", "Turkey", "Iran"]
list_countries = ["France_hosp"]

#THIS THE MAIN FUNCTION
comparison_all_countries(data, list_countries, all_dates, dic_locs, dic_countries_pop)

if False:
    list_countries = ["France", "Italy", "Spain"]
    lower_threshold = 10
    next_x_days_all_except_china = 50
    for country in list_countries:
        print(country)
        confinement = get_confinement(country, all_dates)
        next_x_days = get_next_x_days(country, next_x_days_all_except_china)
        l1, l2 = get_lower_thresholds(country)
        #analysis_country(country, data, next_x_days, confinement, l1, l2, all_dates=all_dates)
        predictions_country(country, data, next_x_days, confinement, l1, l2, all_dates=all_dates)
        print("")
