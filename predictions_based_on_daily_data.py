import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import polynomial, extend, get_scenarios, sigmoid, get_polynomial_trend, get_total_numbers
from scenarios_utils import first_scenario_predictions, third_scenario_predictions

dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}


def plot_predictions_country(country, x_n, y_n, x_t, y_t, removing_last_x_days, next_x_days, confinement, feature, all_dates):
    scenarios = get_scenarios(country)
    subfolder = "predictions" + "/" + country + "/"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    if feature=="tc":
        feature_word, feature_final = "cas", "Nombre de cas" #"Number of cases"
    else:
        feature_word, feature_final = "décès", "Nombre de morts" #"Number of deaths"

    if removing_last_x_days>0:
        x_n, y_n = x_n[:-removing_last_x_days], y_n[:-removing_last_x_days]
    x, y = 1 + x_n - np.amin(x_n), y_n
    
    popt_pol, _ = curve_fit(polynomial, np.log(x), y)
    mid_hist = polynomial(np.log(np.arange(len(x)+next_x_days+1)), popt_pol[0], popt_pol[1], popt_pol[2])
    low_1, mid_1, up_1 = first_scenario_predictions(x, y, y_t[-removing_last_x_days-1], popt_pol, next_x_days, \
        number_of_days_delayed=scenarios[0], days_to_break_the_virus=30, step=0.35)
    low_2, mid_2, up_2 = first_scenario_predictions(x, y, y_t[-removing_last_x_days-1], popt_pol, next_x_days, \
        number_of_days_delayed=scenarios[1], days_to_break_the_virus=30, step=0.5)
    low_3, mid_3, up_3 = first_scenario_predictions(x, y, y_t[-removing_last_x_days-1], popt_pol, next_x_days, \
        number_of_days_delayed=scenarios[2], days_to_break_the_virus=30, step=0.75)
    low_4, mid_4, up_4 = first_scenario_predictions(x, y, y_t[-removing_last_x_days-1], popt_pol, next_x_days, \
        number_of_days_delayed=scenarios[3], days_to_break_the_virus=30, step=2.5)
    
    x, y, x_future = all_dates[x_t], y_t, all_dates[np.arange(np.amax(x_t), np.amax(x_t)+next_x_days+1)]
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    ax.plot(x, y, color="k", marker=".", linestyle = 'None', markersize=15, label="Cas observés jusqu'à aujourd'hui")
    
    if confinement>0:
        ax.axvline(x=all_dates[confinement], color="r", linestyle="dotted", linewidth=5, \
            label="Confinement le " + str(all_dates[confinement]) + " (" + feature_final.lower() + ": " + str(y_t[np.where(confinement==x_t)[0][0]]) + ").")
        begin_date, stop_date  = max(np.where(confinement==x_t)[0][0], 0), min(np.where(confinement==x_t)[0][0]+7, len(x_t)-1)
        num_days = 18
        popt_pol, _ = curve_fit(polynomial, np.log(x_n[begin_date:stop_date]), y_n[begin_date:stop_date])
        
        #popt_pol2, _ = curve_fit(polynomial, np.log(x_n[begin_date:stop_date+20]), y_n[begin_date:stop_date+20])
        
        low1, mid1, up1 = first_scenario_predictions(x_n[begin_date:stop_date], y_n[begin_date:stop_date], y_t[stop_date], popt_pol, num_days, num_days, 30, 0.2)
        _, mid2, _ = third_scenario_predictions(x_n[begin_date:stop_date], y_n[begin_date:stop_date], y_t[stop_date], popt_pol, num_days, \
            number_of_days_delayed=7, number_of_days_same=20, days_to_break_the_virus=30, step=0.0)
        
        ax.plot(all_dates[np.arange(x_t[stop_date], x_t[stop_date]+num_days)], mid1, color="b", linestyle="--", linewidth=7, \
            alpha=0.7, label="Modèle pré-confinement: si on suit le même phénomène qu'en Chine, alors, plus de " + \
            str(int(mid1[-1]/100)*100)+ " le " + str(all_dates[confinement+24])+ ".")
        ax.plot(all_dates[np.arange(x_t[stop_date], x_t[stop_date]+num_days)], mid2, color="r", linestyle="--", linewidth=7, \
            alpha=0.7, label="Modèle pré-confinement: si on suit le même phénomène qu'en Chine, alors, plus de " + \
            str(int(mid2[-1]/100)*100)+ " le " + str(all_dates[confinement+24])+ ".")
    
    if next_x_days>0:
        label1 = "Modèle 1 mis à jour: le nombre de nouveaux "+feature_word+" diminue dans " + str(scenarios[0])+ "jours et atteint 0 slmt 30 jours après: plus de "+ \
            str(int(low_1[-1]/1000)*1000) + "."
        label2 = "Modèle 2 mis à jour: le nombre de nouveaux "+feature_word+" diminue dans " + str(scenarios[1])+ "jours et atteint 0 slmt 30 jours après: plus de "+ \
            str(int(low_2[-1]/1000)*1000) + "."
        label3 = "Modèle 3 mis à jour: le nombre de nouveaux "+feature_word+" diminue dans " + str(scenarios[2])+ "jours et atteint 0 slmt 30 jours après: plus de "+ \
            str(int(low_3[-1]/1000)*1000) + "."
        label4 = "Modèle 4 mis à jour: le nombre de nouveaux "+feature_word+" diminue dans " + str(scenarios[3])+ "jours et atteint 0 slmt 30 jours après: plus de "+ \
        str(int(low_4[-1]/1000)*1000) + "."
        plt.plot(np.concatenate((x, x_future)), mid_hist, alpha= 0.4, color="cyan", linestyle="--", label=label1)
        ax.fill_between(x_future, extend(low_1, len(x_future)), extend(up_1, len(x_future)), alpha= 0.4, color="red", linestyle="--", label=label1)
        ax.fill_between(x_future, extend(low_2, len(x_future)), extend(up_2, len(x_future)), alpha= 0.4, color="orange", linestyle="--", label=label2)
        ax.fill_between(x_future, extend(low_3, len(x_future)), extend(up_3, len(x_future)), alpha= 0.4, color="yellowgreen", linestyle="--", label=label3)
        ax.fill_between(x_future, extend(low_4, len(x_future)), extend(up_4, len(x_future)), alpha= 0.4, color="lime", linestyle="--", label=label4)
        #ax.fill_between(x_future, extend(low_5, len(x_future)), extend(up_5, len(x_future)), alpha= 0.4, color="cyan", linestyle="--", linewidth=7, \
        #label="Modèle: confinement à l'européenne!")
    

    #SIGMOID MODEL
    #x_t = np.arange(len(x_t))
    #x_t_future = np.arange(len(x_t), len(x_t)+next_x_days+1)
    #popt_sig, _ = curve_fit(sigmoid, x_t, y_t, p0=[np.max(y_t)*2, 1., np.argmax(x_n)+10])
    #ax.plot(x_future, sigmoid(x_t_future, popt_sig[0], popt_sig[1], popt_sig[2]), color="r", linestyle="--", linewidth=7, label="Sigmoid model")

    plt.xlabel("Time in days", fontsize=25)
    plt.ylabel(feature_final, fontsize=25)
    xticks = np.concatenate((x, x_future))[np.arange(0, len(np.concatenate((x, x_future))), step=6)]
    plt.xticks(xticks)
    plt.title(country.upper(), fontsize=30)
    lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(1.4, -0.20), fontsize=23, loc=0, prop={'size':25})
    plt.tick_params(axis="both", labelsize=25)
    ax.tick_params(axis='x', rotation=45)
    plt.savefig(subfolder + "/" + country + "_" + feature_final, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()