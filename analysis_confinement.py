import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import polynomial, extend, get_scenarios, sigmoid, get_polynomial_trend, get_total_numbers, exponential, linear, get_stop_day_plot
from scenarios_utils import first_scenario_predictions

dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}

def plot_impact_confinement_country(country, x_n, y_n, x_t, y_t, num_days, confinement, feature, all_dates, l_threshold):
    #TECHNICAL DETAILS
    if confinement>0:
        stop_date = np.where(confinement==x_t)[0][0]
        if stop_date<10:
            #print("Quite an early confinement, this is surprising!")
            stop_date = 10
    else:
        stop_date = min(len(x_n)-1, 10)
    
    subfolder = "analysis" + "/" + country + "/"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    if feature=="tc":
        feature_word, feature_final = "cas", "Nombre de cas" #"Number of cases"
    else:
        feature_word, feature_final = "décès", "Nombre de morts" #"Number of deaths"

    #MODEL PRE CONFINEMENT
    x, y = 1 + x_n - np.amin(x_n), y_n
    popt_exp, _ = curve_fit(exponential, x[:stop_date], y[:stop_date])
    model_exp = np.cumsum(exponential(x, popt_exp[0], popt_exp[1], popt_exp[2]))
    popt_pol, _ = curve_fit(exponential, np.log(x[:stop_date]), y[:stop_date])
    model_pol = np.cumsum(exponential(np.log(x), popt_pol[0], popt_pol[1], popt_pol[2]))

    #MODEL GLOBAL
    global_stop_date = 35
    x, y = 1 + x_n - np.amin(x_n), y_n
    popt_exp2, _ = curve_fit(exponential, x[:global_stop_date], y[:global_stop_date])
    model_exp2 = np.cumsum(exponential(x[:global_stop_date], popt_exp2[0], popt_exp2[1], popt_exp2[2]))
    popt_pol2, _ = curve_fit(exponential, np.log(x[:global_stop_date]), y[:global_stop_date])
    model_pol2 = np.cumsum(exponential(np.log(x[:global_stop_date]), popt_pol2[0], popt_pol2[1], popt_pol2[2]))
    #print(feature_word, round(popt_pol[0], 2), round(popt_pol2[0], 2))
    #print(feature_word, round(popt_exp[0], 2), round(popt_exp2[0], 2))

    x, y = all_dates[x_t], y_t
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    ax.plot(x, y, color="k", marker=".", linestyle = 'None', markersize=18, label="Cas observés jusqu'à aujourd'hui")
    ax.axvline(x=all_dates[x_t[stop_date]], color="k", linestyle=(0, (1, 1)), linewidth=5, alpha=0.6, \
        label="Confinement le " + str(all_dates[x_t[stop_date]]) + " (" + feature_final.lower() + ": " + str(y_t[stop_date]) + ").")
    ax.plot(x[:min(get_stop_day_plot(y, model_exp), global_stop_date)], model_exp[:min(get_stop_day_plot(y, model_exp), global_stop_date)], color="r", \
        linestyle = '--', alpha = 0.5, linewidth=7, label="Model exponential pré confinement, coeff = " + str(round(popt_exp[0], 2)))
    ax.plot(x[:min(get_stop_day_plot(y, model_pol), global_stop_date)], model_pol[:min(get_stop_day_plot(y, model_pol), global_stop_date)], color="b", \
        linestyle = '--', alpha = 0.5, linewidth=7, label="Model polynomial pré confinement, coeff = " + str(round(popt_pol[0], 2)))

    ax.plot(x[:global_stop_date], model_exp2[:global_stop_date], color="r", linestyle = 'dotted', alpha = 0.9, linewidth=6, \
        label="Model exponential all data, coeff = " + str(round(popt_exp2[0], 2)))
    ax.plot(x[:global_stop_date], model_pol2[:global_stop_date], color="b", linestyle = 'dotted', alpha = 0.9, linewidth=6, \
        label="Model polynomial all data, coeff = " + str(round(popt_pol2[0], 2)))

    plt.xlabel("Time in days", fontsize=25)
    plt.ylabel(feature_final, fontsize=25)
    xticks = x[np.arange(0, len(x), step=6)]
    plt.xticks(xticks)
    plt.title(country.upper(), fontsize=30)
    lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(0.4, -0.35), fontsize=23, loc="center", prop={'size':25})
    plt.tick_params(axis="both", labelsize=25)
    ax.tick_params(axis='x', rotation=45)
    plt.savefig(subfolder + "/" + country + "_" + feature_final + "_model_pre_confinement_from_threshold_"+str(l_threshold), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()