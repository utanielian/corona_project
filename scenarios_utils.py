import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit

from utils import polynomial, extend, get_scenarios, sigmoid, get_polynomial_trend, get_total_numbers

dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}


def first_scenario_predictions_aux(x, y, start, next_x_days, function, step=0.5):
    x_future = np.arange(np.amax(x), np.amax(x) + next_x_days)
    popt_pol, _ = curve_fit(polynomial, np.log(x), y)
    up_bound_pol_a, up_bound_pol_b = popt_pol[0]+step, popt_pol[1]-step*np.log(x[-1])
    low_bound_pol_a, low_bound_pol_b = popt_pol[0]-step, popt_pol[1]+step*np.log(x[-1])
    mid_output = polynomial(np.log(x_future), popt_pol[0], popt_pol[1])
    
    lower_bound_output, upper_bound_output = polynomial(np.log(x_future), low_bound_pol_a, low_bound_pol_b), polynomial(np.log(x_future), up_bound_pol_a, up_bound_pol_b)
    low, mid, up = get_total_numbers(start, lower_bound_output), get_total_numbers(start, mid_output), get_total_numbers(start, upper_bound_output)
    return low, mid, up, lower_bound_output, upper_bound_output


def first_scenario_predictions(x, y, start, popt, next_x_days, number_of_days_delayed, days_to_break_the_virus, step):
    #1
    low, mid, up, low1, mid1, up1 = get_polynomial_trend(x, start, popt, number_of_days_delayed, step)
    if next_x_days <= number_of_days_delayed:
        return low1, mid1, up1
    #2
    else:
        low, mid, up = np.linspace(low[-1], 0., days_to_break_the_virus)[1:], \
            np.linspace(mid[-1], 0., days_to_break_the_virus)[1:], \
            np.linspace(up[-1], 0., days_to_break_the_virus)[1:]
        low2, mid2, up2 = get_total_numbers(low1[-1], low), get_total_numbers(mid1[-1], mid), get_total_numbers(up1[-1], up)
        l, m, u = np.concatenate((low1, low2))[:next_x_days], np.concatenate((mid1, mid2))[:next_x_days], np.concatenate((up1, up2))[:next_x_days]
        return l, m, u

def second_scenario_predictions(x, y, start, next_x_days, num_days_same, days_to_break_the_virus, step):
    #1
    last_y = np.mean(y[-2:])
    low, up, mid = np.ones(num_days_same)*(last_y - step*last_y), np.ones(num_days_same)*(last_y + step*last_y), \
        np.ones(num_days_same)*last_y
    low1, up1, mid1 = get_total_numbers(start, low), get_total_numbers(start, up), get_total_numbers(start, mid)
    if next_x_days <= num_days_same:
        return low1[:next_x_days], up1[:next_x_days], mid1[:next_x_days]
    #2
    else:
        daily_fut = np.linspace(last_y, 0., days_to_break_the_virus)[1:]
        low2, up2, mid2 = get_total_numbers(low1[-1], daily_fut), get_total_numbers(up1[-1], daily_fut), get_total_numbers(mid1[-1], daily_fut)
        l, m, u = np.concatenate((low1, low2))[:next_x_days], np.concatenate((mid1, mid2))[:next_x_days], np.concatenate((up1, up2))[:next_x_days]
        return l, m, u
    

def third_scenario_predictions(x, y, start, popt, next_x_days, number_of_days_delayed, number_of_days_same, days_to_break_the_virus, step):
    #1
    low, mid, up, low1, mid1, up1 = get_polynomial_trend(x, start, popt, number_of_days_delayed, step)
    if next_x_days <= number_of_days_delayed:
        return low1, mid1, up1
    #2
    low2, mid2, up2 = get_total_numbers(low1[-1], np.ones(number_of_days_same)*low[-1]), get_total_numbers(mid1[-1], np.ones(number_of_days_same)*mid[-1]), \
        get_total_numbers(up1[-1], np.ones(number_of_days_same)*up[-1])
    if next_x_days <= number_of_days_same:
        return np.concatenate((low1, low2))[:next_x_days], np.concatenate((mid1, mid2))[:next_x_days], np.concatenate((up1, up2))[:next_x_days]
    #3
    else:
        low, mid, up = np.linspace(low[-1], 0., days_to_break_the_virus)[1:], np.linspace(low[-1], 0., days_to_break_the_virus)[1:], \
            np.linspace(low[-1], 0., days_to_break_the_virus)[1:]
        low3, mid3, up3 = get_total_numbers(low2[-1], low), get_total_numbers(mid2[-1], mid), get_total_numbers(mid2[-1], up)
        return np.concatenate((low1, low2, low3))[:next_x_days], np.concatenate((mid1, mid2, mid3))[:next_x_days], \
            np.concatenate((up3, up2, up3))[:next_x_days]
