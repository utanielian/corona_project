import numpy as np
import math
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata
from scipy.optimize import curve_fit
import datetime
import sys

dico_from_colnames_to_index = {'dates':0, "loc":1, "nc":2, "nd":3, "tc":4, "td":5}

def extend(a, length):
    if len(a)>= length:
        return a
    else:
        b = np.zeros(length)
        b[np.arange(len(a))] = a
        for i in range(len(a), len(b)):
            b[i] = a[-1]
        return b

def linear(x, a, b, c):
    return a*x + b

def parabola(x, a, b, c):
    return a*x*x + b*x + 0

def cubic(x, a, b, c):
    return a*x*x*x + b*x*x + c*x

def exponential(x, a, b, c):
    return np.exp(a*x)+b

def exponential_minus(x, a, b, c):
    return -np.exp(a*x+b) + c

def exponential_just_a(x, a, b=0, c=1):
    return np.exp(a*x)

def exponential_single(x, a, b=0, c=1):
    return np.exp(a*x)*b

def exponential_with_b_inside(x, a, b, c):
    return np.exp(a*x+b)

def exponential_coefficient_square(x, a, b, c):
    return np.exp(a*np.power(x, 1.25) + b*x + c) + 1

def polynomial(x, a, b, c=1):
    return np.exp(a*x+b)

def exponential_coefficient(x, a, b, c=1):
    return np.exp(a*x+b) + 1

def logfunc_exponential_coefficient(x, a, b, c=1):
    return np.log(np.exp(a*x+b) + 1)

def normal(x, a, b=1, c=1):
    return (1/(a*math.sqrt(2*math.pi)))*np.exp(-(x-b)**2/(2.*a**2))

def lognormal(x, a, b, c):
    return np.log((1/(a*math.sqrt(2*math.pi)))*np.exp(-(x-b)**2/(2.*a**2)))

def log_normal(x, a, b, c):
    return (1/(x*a*math.sqrt(2*math.pi)))*np.exp(-(np.log(x)-b)**2/(2.*a**2))

def loglog_normal(x, a, b, c):
    return np.log((1/(x*a*math.sqrt(2*math.pi)))*np.exp(-(np.log(x)-b)**2/(2.*a**2)))

def cauchy(x, a, b, c):
    return 1/(np.pi*a*(1+((x-b)/a)**2))

def logcauchy(x, a, b, c):
    return np.log(1/(np.pi*a*(1+((x-b)/a)**2)))

def sigmoid(x, a, b, c):
    return a/(1+np.exp(-b*(x-c)))

def movingaverage(values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

def get_data_for_a_country_bis(country_index, data, feature, lower_threshold=1, upper_threshold=1000000):
    data_country = data[np.where((data[:,dico_from_colnames_to_index["loc"]]==country_index))]
    x, y = data_country[:,dico_from_colnames_to_index["dates"]], data_country[:,dico_from_colnames_to_index[feature]]
    return x, y

def get_data_for_a_country(country_index, data, feature, lower_threshold=1, upper_threshold=1000000):
    data_country = data[np.where((data[:,dico_from_colnames_to_index["loc"]]==country_index))[0]]
    if feature == "nc" or feature == "tc":
        index1 = min(np.where(data_country[:,dico_from_colnames_to_index["tc"]]>=lower_threshold)[0])
        index2 = max(np.where(data_country[:,dico_from_colnames_to_index["tc"]]<=upper_threshold)[0])
    else :
        index1 = min(np.where(data_country[:,dico_from_colnames_to_index["td"]]>=lower_threshold)[0])
        index2 = max(np.where(data_country[:,dico_from_colnames_to_index["td"]]<=upper_threshold)[0])
    x, y = data_country[:,dico_from_colnames_to_index["dates"]][index1:index2], data_country[:,dico_from_colnames_to_index[feature]][index1:index2]
    return x, y
    
def plot_analysis_country(x, y, country_name, feature, dic_locs, ma=1, next_x_days=6):
    subfolder = "analysis" + "/" + country_name + "/"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    if feature=="tc":
        label_y = "Total cases"
    elif feature =="td":
        label_y = "Total deaths"
    elif feature =="nc":
        label_y = "New cases"
    elif feature =="nd":
        label_y = "New deaths"

    x = 1 + x - np.amin(x)
    x, y = x[ma-1:], movingaverage(y, ma)

    plt.clf()
    f = plt.figure(figsize=(10,8))
    ax = f.add_subplot(111)
    #1) Get current polynomial fit
    
    #error = np.sum(np.abs((y-polynomial(np.log(x), popt_pol[0], popt_pol[1], popt_pol[2]))))
    
    #2) Plot data and current fit
    plt.plot(x, y, label=label_y, color="k", linewidth=3)
    popt_pol, _ = curve_fit(polynomial, np.log(x), y)
    plt.plot(x, polynomial(np.log(x), popt_pol[0], popt_pol[1], popt_pol[2]), color="b", label="Model polynomial")
    popt_exp, _ = curve_fit(exponential, x, y, method="dogbox", maxfev=5000)
    plt.plot(x, exponential(x, popt_exp[0], popt_exp[1], popt_exp[2]), color="r", label="Model exponential")
    
    #3) projections for the next five days
    today = np.amax(x)
    plt.axvline(x=today, color="r", linestyle="dotted", label="Today")
    x = np.arange(today, today+next_x_days)
    #Scenario 1: nothing changes!
    step = 0.3
    up_a, up_b, up_c = popt_pol[0]+step, popt_pol[1]-step*np.log(today), popt_pol[2]
    low_a, low_b, low_c = popt_pol[0]-step, popt_pol[1]+step*np.log(today), popt_pol[2]
    plt.fill_between(x, polynomial(np.log(x), up_a, up_b, up_c), polynomial(np.log(x), low_a, low_b, low_c), \
        alpha= 0.5, color="b", linestyle="--", label="If nothing changes")
    plt.plot(x, exponential(x, popt_exp[0], popt_exp[1], popt_exp[2]), alpha= 0.75, color="r", linestyle="--", label="Exponential prediction")

    plt.text(0.75, 0.05, "Best fit: days^" + str(round(popt_pol[0],2)), size=20, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    plt.xlabel("Time in days", fontsize=18)
    plt.ylabel(label_y, fontsize=18)
    plt.title(country_name + " : " + label_y, fontsize=23)
    plt.legend(loc=2, prop={'size':17})
    plt.tick_params(axis="both", labelsize=17)
    plt.savefig(subfolder + "/" + country_name+"_"+feature)
    plt.close()
    print(feature + " degree = ", round(popt_pol[0], 1))


def does_the_x_previous_days_can_predict_today(x, y, ma=1, number_previous_days=10):
    x = 1 + x - np.amin(x)
    x, y = x[ma-1:], movingaverage(y, ma)
    popt_pol, _ = curve_fit(polynomial, np.log(x[-number_previous_days:-1]), y[-number_previous_days:-1])
    pred_yday = int(polynomial(np.log(x[-1]), popt_pol[0], popt_pol[1], popt_pol[2]))
    relative_error_yday = round(100*(y[-1]-pred_yday)/y[-1], 1)
    print("Yesterday's mistake = ", relative_error_yday, "pred", pred_yday, "real", y[-1])
    print("Tomorrow's prediction = ", int(polynomial(np.log(x[-1]+1), popt_pol[0], popt_pol[1], popt_pol[2])))


def plot_data_by_country_tc_vs_nc(country_name, feature1, feature2, data, dic_locs, threshold):
    data_country = get_data_for_a_country(dic_locs[country_name], feature1, data, threshold)
    plt.clf()
    f = plt.figure()
    ax = f.add_subplot(111)
    x, y = data_country[:,dico_from_colnames_to_index["tc"]], data_country[:,dico_from_colnames_to_index["nc"]]
    popt_par, _ = curve_fit(parabola, x, y)
    popt_cub, _ = curve_fit(cubic, x, y)
    popt_lin, _ = curve_fit(linear, x, y)
    plt.plot(x, y, label="Data", color="k", linestyle="--", linewidth=3)
    plt.plot(x, linear(x, popt_lin[0], popt_lin[1], popt_lin[2]), color="g", label="Linear approximation")
    plt.xlabel("Total cases")
    plt.ylabel("New cases")
    plt.text(0.7, 0.05, "coef_dir=" + str(round(popt_lin[0],2)), size=20, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)
    plt.title(country_name + " : " + feature1 + "_" + feature2)
    plt.legend(loc=2)
    plt.savefig(country_name+"_" + feature1 + "_" + feature2)
    plt.close()
    return popt_lin, popt_par ,popt_cub


def plot_comparison_cases_vs_deaths_by_country(country_name, data, dic_locs, threshold):
    data_country = get_data_for_a_country(dic_locs[country_name], "tc", data, threshold)
    plt.clf()
    plt.plot(data_country[:,dico_from_colnames_to_index["dates"]] - np.amin(data_country[:,dico_from_colnames_to_index["dates"]]), \
        data_country[:,dico_from_colnames_to_index["tc"]], label="Total cases", linewidth=2)
    plt.plot(data_country[:,dico_from_colnames_to_index["dates"]] - np.amin(data_country[:,dico_from_colnames_to_index["dates"]]), \
        data_country[:,dico_from_colnames_to_index["td"]], label="Total cases", linewidth=2)
    plt.savefig("comparison_"+country_name)
    plt.close()


def get_all_dates(start_year, start_month, start_day, total_number_of_days):
    beginning = datetime.datetime(start_year, start_month, start_day)
    date = beginning
    all_dates, all_dates_year = list(), list()
    for i in range(total_number_of_days): 
        date += datetime.timedelta(days=1)
        all_dates.append(date.strftime("%d-%m-%Y"))
        all_dates_year.append(date.strftime("%Y-%m-%d"))
    return np.array(all_dates), np.array(all_dates_year)


def get_confinement(country, all_dates):
    if country=="Italy":
        return np.where(all_dates=="09-03-2020")[0][0]
    elif country=="France":
        return np.where(all_dates=="17-03-2020")[0][0]
    elif country=="Spain":
        return np.where(all_dates=="14-03-2020")[0][0]
    elif country=="China":
        return np.where(all_dates=="23-01-2020")[0][0]
    elif country=="USA":
        return np.where(all_dates=="22-03-2020")[0][0]
    elif country=="Germany":
        return np.where(all_dates=="22-03-2020")[0][0]
    else:
        return np.where(all_dates=="22-03-2020")[0][0]
    
def get_scenarios(country):
    if country=="Italy":
        return [11, 8, 5, 2]
    elif country=="Spain":
        return [14, 11, 8, 5]
    elif country=="France":
        return [15, 12, 9, 6]
    else:
        return [9, 6, 3, 1]

def get_next_x_days(country, next_x_days=50):
    if country=="China" or country=="South Korea":
        return next_x_days
    else:
        return next_x_days

def get_lower_thresholds(country):
    if country=="China":
        return 250, 25
    elif country=="World":
        return 90000, 50
    else:
        return 250, 25

def get_total_numbers(start, updates):
    numbers = list()
    s = start
    for elem in updates:
        s += elem
        numbers.append(s)
    return np.array(numbers)

def get_exponential_trend(x, start, popt, number_of_days_delayed, step):
    x_future = np.arange(np.amax(x), np.amax(x) + number_of_days_delayed)
    up_a, up_b = popt[0]+step, popt[1]-step*np.log(x[-1])
    low_a, low_b = popt[0]-step, popt[1]+step*np.log(x[-1])
    low_n, mid_n, up_n = exponential(x_future, low_a, low_b), exponential(x_future, popt[0], popt[1]), exponential(x_future, up_a, up_b)
    low_t, up_t, mid_t = get_total_numbers(start, low_n), get_total_numbers(start, up_n), get_total_numbers(start, mid_n)
    return low_n, mid_n, up_n, low_t, mid_t, up_t

def get_polynomial_trend(x, start, popt, number_of_days_delayed, step):
    x_future = np.arange(np.amax(x), np.amax(x) + number_of_days_delayed)
    up_a, up_b = popt[0]+step, popt[1]-step*np.log(x[-1])
    low_a, low_b = popt[0]-step, popt[1]+step*np.log(x[-1])
    low_n, mid_n, up_n = polynomial(np.log(x_future), low_a, low_b), polynomial(np.log(x_future), popt[0], popt[1]), polynomial(np.log(x_future), up_a, up_b)
    low_t, up_t, mid_t = get_total_numbers(start, low_n), get_total_numbers(start, up_n), get_total_numbers(start, mid_n)
    return low_n, mid_n, up_n, low_t, mid_t, up_t

def get_stop_day_plot(y_1, y_2):
    error_relative = (y_2-y_1)/y_1
    threshold = 2.5
    if len(np.where(error_relative>threshold)[0])==0:
        return len(y_1)
    else:
        return np.amin(np.where(error_relative>threshold)[0])


def get_data_country_aux(country, data, feature, mode_data, all_dates, dic_locs):
    l_cases, l_deaths = get_lower_thresholds(country)
    if feature == "tc":
        feature1, feature2 = "nc", "tc"
        l = l_cases
    else:
        feature1, feature2 = "nd", "td"
        l = l_deaths

    if mode_data=="confinement":
        x_n, y_n = get_data_for_a_country(dic_locs[country], data, feature=feature1, lower_threshold=1, upper_threshold=10000000)
        x_t, y_t = get_data_for_a_country(dic_locs[country], data, feature=feature2, lower_threshold=1, upper_threshold=10000000)
        confinement = get_confinement(country, all_dates)
        if confinement<x_t[0]:
            print(country.upper() + ": confinement before first case issue")
            return x_n, y_n, x_t, y_t
        else:
            return x_n[np.where(x_t==confinement)[0][0]:], y_n[np.where(x_t==confinement)[0][0]:], \
                x_t[np.where(x_t==confinement)[0][0]:], y_t[np.where(x_t==confinement)[0][0]:]
        
    elif mode_data=="threshold":
        x_n, y_n = get_data_for_a_country(dic_locs[country], data, feature=feature1, lower_threshold=l, upper_threshold=500000)
        x_t, y_t = get_data_for_a_country(dic_locs[country], data, feature=feature2, lower_threshold=l, upper_threshold=500000)
        length = min(len(x_n), len(x_t))
        return x_n[-length:], y_n[-length:], x_t[-length:], y_t[-length:]
    else:
        x_n, y_n = get_data_for_a_country(dic_locs[country], data, feature=feature1, lower_threshold=1, upper_threshold=10000000)
        x_t, y_t = get_data_for_a_country(dic_locs[country], data, feature=feature2, lower_threshold=1, upper_threshold=10000000)
        length = min(len(x_n), len(x_t))
        return x_n[-length:], y_n[-length:], x_t[-length:], y_t[-length:]


def get_all_pred_models(popt_exp, popt_exp_low, popt_exp_up, gmax, start_value_y, start_value_y_n, stop_date, step):
    model_prev, low_prev, up_prev = \
            exponential_coefficient(np.arange(gmax), popt_exp[0], popt_exp[1], popt_exp[2]), \
            exponential_coefficient(np.arange(gmax), popt_exp_low[0], popt_exp_low[1], popt_exp[2]), \
            exponential_coefficient(np.arange(gmax), popt_exp_up[0], popt_exp_up[1], popt_exp[2])
    
    model_prev_total, low_prev_total, up_prev_total = \
        start_value_y*np.cumprod(model_prev[stop_date:]), \
        start_value_y*np.cumprod(low_prev[stop_date:]), \
        start_value_y*np.cumprod(up_prev[stop_date:])
    
    model_prev_diff, low_prev_diff, up_prev_diff = \
        model_prev_total[1:]-model_prev_total[:-1] + start_value_y_n - (model_prev_total[1]-model_prev_total[0]), \
        low_prev_total[1:]-low_prev_total[:-1] + start_value_y_n - (model_prev_total[1]-model_prev_total[0]), \
        up_prev_total[1:]-up_prev_total[:-1] + start_value_y_n - (model_prev_total[1]-model_prev_total[0])
    return model_prev, low_prev, up_prev, model_prev_total, low_prev_total, up_prev_total, model_prev_diff, low_prev_diff, up_prev_diff

def get_the_biggest_minust_five_pct(y1, y2, pct=0.05):
    if np.amax(y1)>10*np.amax(y2):
        return y2*(1+pct)
    elif np.amax(y2)>10*np.amax(y1):
        return y1*(1+pct)
    elif np.amax(y1)>np.amax(y2):
        return y1*(1+pct)
    else:
        return y2*(1+pct)


def get_the_smallest_minust_five_pct(y1, y2, pct=0.05):
    if np.amax(y1)<np.amax(y2):
        return y1*(1-pct)
    else:
        return y2*(1-pct)

def get_lower_and_upper_bounds(popt_exp, popt_china, popt_italy, beg_date, stop_date, step, length):
    low_exp0, up_exp0 = popt_exp[0]-step, popt_exp[0]+step
    if popt_exp[0]<popt_china[0]:
        up_exp0 = max(up_exp0, popt_china[0]+step)
    if popt_exp[0]>popt_italy[0]/1.5:
        low_exp0 = min(low_exp0, popt_exp[0] + -np.abs(popt_italy[0]-popt_exp[0])/2)
    if popt_exp[0]>popt_italy[0]/2:
        low_exp0 = min(up_exp0, popt_italy[0]/1.5-step)
        up_exp0 = min(up_exp0, popt_italy[0]/2.00+step)
    if popt_exp[0]<popt_china[0]*1.5:
        low_exp0 = max(low_exp0, popt_china[0]*1.75)
        up_exp0 = max(up_exp0, popt_china[0]*1.25)
    popt_exp0 = (low_exp0+up_exp0)/2
    return popt_exp0, low_exp0, up_exp0