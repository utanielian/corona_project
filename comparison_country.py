import numpy as np
import csv
import requests
import matplotlib.pyplot as plt
import os
from scipy.stats import rankdata, norm, lognorm, gamma
from scipy.optimize import curve_fit
from matplotlib import rc
from datetime import datetime, timedelta, date
import math

from utils import polynomial, extend, get_scenarios, sigmoid, get_polynomial_trend, get_total_numbers, exponential, exponential_with_b_inside, \
    logfunc_exponential_coefficient, get_stop_day_plot, exponential_coefficient, movingaverage, exponential_coefficient_square, linear, get_data_country_aux, \
    get_all_pred_models, get_the_smallest_minust_five_pct, get_the_biggest_minust_five_pct, exponential_minus, get_lower_and_upper_bounds, \
    normal, lognormal, cauchy, logcauchy, log_normal, loglog_normal


def plot_comparison_all_countries(data, list_countries, feature, mode_data, trend_boolean, all_dates, dic_locs, step=0.005, bc=1.25, wc=1.75, gmax=52, trend=5):
    today_minust_two_days = date.today() - timedelta(days=2)
    today, yesterday, daybefore = date.today(), date.today()-timedelta(days=1), date.today()-timedelta(days=2)
    subfolder = "comparisons_" +"_".join(list_countries) + "/" + "pred_with_trend"  + "/" if trend_boolean else "comparisons_"+"_".join(list_countries) + "/" + "pred_with_no_trend" + "/"
    feature_word = "cases" if feature=="tc" else "deaths"

    if mode_data=="confinement":
        subfolder += "confinement"
        title = "Number of " +feature_word+" starting from confinement. Model as of " + today_minust_two_days.strftime("%d/%m/%Y")+ "."
    else:
        subfolder += "no_confinement"
        title = "Number of " +feature_word+". Model as of " + today_minust_two_days.strftime("%d/%m/%Y") + "."
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    print("")
    plt.clf()
    fig, axs = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=False, figsize=(100,60), dpi=50)
    plt.subplots_adjust(hspace=0.4, top=0.85, right=0.50)
    cmap = plt.cm.get_cmap('jet')
    colors = ["black", "red", "green", "orange", "blue", "cyan"]

    #GETTING VALUES FOR CHINA AND ITALY...
    def get_exponential_factor_aux(country, data, feature, mode_data, all_dates, dic_locs, ma=4):
        _, _, x, y = get_data_country_aux(country, data, feature, mode_data, all_dates, dic_locs)
        y_ratio = y[1:]/y[:-1]
        y_ratio = movingaverage(y_ratio, ma)
        beg_date, stop_date = np.argmax(y_ratio), min(len(y_ratio)-2, 60)
        popt_rel = curve_fit(linear, np.arange(stop_date-beg_date), np.log(y_ratio[beg_date:stop_date]-0.99999), method="dogbox", maxfev=5000)[0]
        popt_s = curve_fit(exponential_coefficient, np.arange(stop_date-beg_date), y_ratio[beg_date:stop_date], method="dogbox", maxfev=5000)[0]
        return popt_s, popt_rel, beg_date, stop_date+ma

    def get_exponential_factor(country, data, feature, mode_data, all_dates, dic_locs, popt_china, popt_italy):
        popt_s, popt_rel, beg_date, stop_date = get_exponential_factor_aux(country, data, feature, mode_data, all_dates, dic_locs)
        mid_rel0, low_rel0, up_rel0 = get_lower_and_upper_bounds(popt_rel, popt_china, popt_italy, beg_date, stop_date, step, len(y_ratio))
        mid_s0, low_s0, up_s0 = get_lower_and_upper_bounds(popt_s, popt_china, popt_italy, beg_date, stop_date, step, len(y_ratio))
        popt_s[0], popt_rel[0] = mid_s0, mid_rel0

        if trend_boolean:
            popt_s[1] = np.log(np.mean(y_ratio[-trend:])-0.99999) - popt_s[0]*(stop_date-beg_date)
            popt_rel[1] = np.log(np.mean(y_ratio[-trend:])-0.99999) - popt_rel[0]*(stop_date-beg_date)
            low_rel1 = np.log(np.mean(y_ratio[-trend:])-0.99999) - low_rel0*(stop_date-beg_date)
            up_rel1 = np.log(np.mean(y_ratio[-trend:])-0.99999) - up_rel0*(stop_date-beg_date)
            low_s1 = np.log(np.mean(y_ratio[-trend:])-0.99999) - low_s0*(stop_date-beg_date)
            up_s1 = np.log(np.mean(y_ratio[-trend:])-0.99999) - up_s0*(stop_date-beg_date)
        else:
            low_s1, up_s1, low_rel1, up_rel1 = popt_s[1], popt_s[1], popt_rel[1], popt_rel[1]
        return popt_s, popt_rel, [low_s0, low_s1], [up_s0, up_s1], [low_rel0, low_rel1], [up_rel0, up_rel1], beg_date, stop_date

    popt_china, popt_china_rel, _, _ = get_exponential_factor_aux("China", data, feature, mode_data, all_dates, dic_locs)
    popt_italy, popt_italy_rel, _, _ = get_exponential_factor_aux("Italy", data, feature, mode_data, all_dates, dic_locs)
    print("All china", popt_china[0], "All italy", popt_italy[0])
    extra_artists = list()
    max_total_numbers = list()

    for i, country in enumerate(list_countries):
        c = colors[i]
        _, y_n, x, y = get_data_country_aux(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        y_ratio = y[1:]/y[:-(1)]
        axs[0].plot(np.arange(len(y)-1), y_ratio, linewidth=7, marker="o", markersize=10, c=c)
        axs[1].plot(np.arange(len(y_n)), y_n, linewidth=7, linestyle=None, marker="o", markersize=8, c=c)
        axs[2].plot(np.arange(len(y)), y, linewidth=7, linestyle=None, marker="o", markersize=8, c=c, label=country.upper())

        if len(y_ratio)<5 or np.argmax(movingaverage(y_ratio, 4))>len(movingaverage(y_ratio, 4))-2:
            print(country + " is still too soon to call ...")
            extra_artists.append(plt.text(0.97, -1.80+0.15*i, "Still too soon to call for " + country.upper(), size=70, \
                horizontalalignment='left', transform = axs[1].transAxes))
            continue
        if len(y_ratio)>50 or np.mean(y_n[-3:]<2):
            print(country + " has beatten the coronavirus...")
            continue

        popt_exp, popt_exp_rel, popt_exp_low, popt_exp_up, popt_exp_rel_low, popt_exp_rel_up, beg_date, stop_date = \
            get_exponential_factor(list_countries[i], data, feature, mode_data, all_dates, dic_locs, popt_china, popt_italy)
        print(country, popt_exp[0], popt_exp_low[0], popt_exp_up[0])
        print(country, popt_exp_rel[0], popt_exp_rel_low[0], popt_exp_rel_up[0])

        model_prev, low_prev, up_prev, model_prev_total, low_prev_total, up_prev_total, model_prev_diff, low_prev_diff, up_prev_diff = \
            get_all_pred_models(popt_exp, popt_exp_low, popt_exp_up, gmax, y[stop_date], y_n[stop_date], stop_date, step)

        _, _, _, model_prev_total_rel, low_prev_total_rel, up_prev_total_rel, model_prev_diff_rel, low_prev_diff_rel, up_prev_diff_rel = \
            get_all_pred_models(popt_exp_rel, popt_exp_rel_low, popt_exp_rel_up, gmax, y[stop_date], y_n[stop_date], stop_date, step)
            
        low_prev_total, low_prev_diff, up_prev_total, up_prev_diff = get_the_smallest_minust_five_pct(low_prev_total, low_prev_total_rel), \
            get_the_smallest_minust_five_pct(low_prev_diff, low_prev_diff_rel), get_the_biggest_minust_five_pct(up_prev_total, up_prev_total_rel), \
            get_the_biggest_minust_five_pct(up_prev_diff, up_prev_diff_rel)

        max_total_numbers.append(low_prev_total[-1])
        
        #PLOTTING THE MODELS
        axs[0].plot(np.arange(gmax)[stop_date:], low_prev[stop_date:], linewidth=9, linestyle="dotted", c=c)
        axs[0].plot(np.arange(gmax)[stop_date:], up_prev[stop_date:], linewidth=9, linestyle="dotted", c=c)
        axs[1].plot(np.arange(gmax)[stop_date:-1], low_prev_diff, linewidth=9, c=c, linestyle="dotted")
        axs[1].plot(np.arange(gmax)[stop_date:-1], up_prev_diff, linewidth=9, c=c, linestyle="dotted")
        axs[2].plot(np.arange(gmax)[stop_date:], low_prev_total, linewidth=9, c=c, linestyle="dotted")
        axs[2].plot(np.arange(gmax)[stop_date:], up_prev_total, linewidth=9, c=c, linestyle="dotted")
        extra_artists.append(plt.text(1.05, 2.20-0.12*(i+1), country.upper()[:3] + " from "+ str(round(popt_exp_up[0], 3)) +" to "+ str(round(popt_exp_low[0], 3)) , size=69, horizontalalignment='left', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.05, 0.95-0.12*(i+1), country.upper()[:3], size=69, horizontalalignment='left', transform = axs[1].transAxes)) 
        extra_artists.append(plt.text(1.25, 0.95-0.12*(i+1), str(int(y_n[-2]))+ "--" + str(int((low_prev_diff[0]))) + ":" + str(int((up_prev_diff[0]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.50, 0.95-0.12*(i+1), str(int(y_n[-1]))+ "--" + str(int((low_prev_diff[1]))) + ":" + str(int((up_prev_diff[1]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.75, 0.95-0.12*(i+1), str(int((low_prev_diff[2]))) + ":" + str(int((up_prev_diff[2]))), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        
        extra_artists.append(plt.text(1.05, -0.75-0.12*(i+1), country.upper()[:3], size=69, horizontalalignment='left', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.20, -0.75-0.12*(i+1), str(int(y[-1])), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.42, -0.75-0.12*(i+1), str(int(low_prev_total[7]/100)*100) + " to " + str(int(up_prev_total[7]/100)*100), size=69, horizontalalignment='center', transform = axs[1].transAxes))
        extra_artists.append(plt.text(1.70, -0.75-0.12*(i+1), str(int(low_prev_total[-1]/100)*100) + " to " + str(int(up_prev_total[-1]/100)*100), size=69, horizontalalignment='center', transform = axs[1].transAxes))

        if np.amax(y_n)<np.amax(model_prev_diff):
            maxx, peak_day = int(np.amax(model_prev_diff)), all_dates[x[stop_date]+np.argmax(model_prev_diff)]
        else:
            maxx, peak_day = int(np.amax(y_n)), all_dates[x[np.argmax(y_n)]]
        plt.text(1.05, 0.30-0.12*(i+1), country.upper()[:3] + "  "+ peak_day + " with " + str(maxx) + " " + feature_word, size=69, \
            horizontalalignment='left', transform = axs[1].transAxes)

    plt.xlabel("Days after confinement", fontsize=85)
    plt.text(0.05, 2.50, "1) Growth w.r.t. the previous day", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.05, 1.10, "2) Evolution of the number of daily " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.05, -0.30, "3) Evolution of the total number of " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="blue")
    plt.text(0.40, 2.2, "Notation: " + r"$S_t$ = " + "number of total " + feature_word + ".", size=85, horizontalalignment='left', transform = axs[1].transAxes)
    plt.text(0.40, 2., "Assumption: " + r"$S_{t+1}/S_t = e^{-\alpha t + \beta} + 1$.", size=85, horizontalalignment='left', transform = axs[1].transAxes)
    plt.text(0.40, 1.8, "Approach: we learn both " + r"$\alpha$" +" and " + r"$\beta$", size=85, horizontalalignment='left', transform = axs[1].transAxes)
    plt.text(1.05, 2.20, "Country's "+ r"$\alpha$"+ " parameter", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red")
    extra_artists.append(plt.text(1.05, 1.15, "Daily number of " + feature_word, horizontalalignment='left', transform = axs[1].transAxes, color="red", size=85))
    #extra_artists.append(plt.text(1.05, 1.18, "Dates",horizontalalignment='left', transform = axs[1].transAxes, color="red", size=85))
    extra_artists.append(plt.text(1.25, 1.05, daybefore.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.50, 1.05, yesterday.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.75, 1.05, today.strftime("%d/%m"), size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.25, 0.95, "data --- past ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.50, 0.95, "data --- past ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.75, 0.95, "future ests.", size=70, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.05, 0.30, "Estimation of the peak day", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    extra_artists.append(plt.text(1.05, -0.65, "Total number of "+feature_word, size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    extra_artists.append(plt.text(1.20, -0.75, "Today " + today.strftime("%d/%m"), size=69, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.42, -0.75, "in 5 days", size=69, horizontalalignment='center', transform = axs[1].transAxes))
    extra_artists.append(plt.text(1.70, -0.75, "in 50 days", size=69, horizontalalignment='center', transform = axs[1].transAxes))
    #extra_artists.append(plt.text(1.05, -0.40, "Estimations +5 days", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    #extra_artists.append(plt.text(1.05, -1.10, "Estimations +50 days", size=85, horizontalalignment='left', transform = axs[1].transAxes, color="red"))
    if "USA" in list_countries:
        extra_artists.append(plt.text(0.97, -1.80, "The confinement date used for USA is the 23/03/2020.", size=70, horizontalalignment='left', transform = axs[1].transAxes))
    
    #TECHNICAL DETAILS FOR THE PLOT
    axs[0].set_ylabel(r"$S_{t+1}/S_t$", fontsize=85)
    axs[1].set_ylabel(r"$S_{t+1}-S_t$", fontsize=85)
    axs[2].set_ylabel(r"$S_t$", fontsize=85)
    plt.xticks(np.arange(0, gmax, step=4))
    extra_artists.append(plt.suptitle(title, x=0.40, y=0.95, fontsize=100))
    extra_artists.append(plt.legend(frameon=False, ncol=len(list_countries), borderaxespad=0., bbox_to_anchor=(0.5, -0.40), \
        fontsize=23, loc="center", labelspacing=0.9, prop={'size':80}))
    axs[0].tick_params(axis="both", labelsize=80)
    axs[0].tick_params(axis='x', rotation=45)
    axs[1].tick_params(axis="both", labelsize=80)
    axs[1].tick_params(axis='x', rotation=45)
    axs[2].tick_params(axis="both", labelsize=80)
    axs[2].tick_params(axis='x', rotation=45)
    if len(list_countries)>1:
        axs[1].set_yscale("log")
        axs[2].set_yscale("log")

    fig.savefig(subfolder + "/" + "prediction_countries_"+feature_word+"_"+"_".join(list_countries), bbox_extra_artists=extra_artists, bbox_inches='tight')
    fig.savefig(subfolder + "/" + "prediction_countries_"+feature_word+"_"+"_".join(list_countries)+".pdf", bbox_extra_artists=extra_artists, bbox_inches='tight')
    #extent = axs[2].get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    #fig.savefig(subfolder + "/" + feature, bbox_inches=extent.expanded(1.1, 2.))
    plt.close()
    

def plot_comparison_growth_all_countries(data, list_countries, feature, step, pred=False):
    subfolder = "comparisons_" +"_".join(list_countries) + "/" + "comparisons_speed"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    if feature=="tc":
        feature = "cases"
    else:
        feature = "deaths"

    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    minn, maxx, max_length = min([min(elem) for elem in data_countries]), max([max(elem) for elem in data_countries]), max([len(elem) for elem in data_countries])
    global_max = max_length + 4
    cmap = plt.cm.get_cmap('jet')
    for i, country in enumerate(list_countries):
        c = cmap(i/len(list_countries))
        _, y_n, x, y = get_data_country_aux(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        plt.plot(np.arange(len(data_countries[i])), data_countries[i], linewidth=9, linestyle="solid", label=country.upper(), marker="o", markersize=10, c=c)
        if pred:
            popt_exp, _ = curve_fit(exponential_with_b_inside, np.arange(len(data_countries[i])), data_countries[i], method="dogbox", maxfev=5000)
            model_prev = exponential_with_b_inside(np.arange(global_max), popt_exp[0], popt_exp[1], popt_exp[2])
            if popt_exp[0]<0:
                plt.plot(np.arange(global_max), model_prev, linewidth=9, linestyle="dotted", c=c)

    plt.xlabel("Stages of " + str(step) + " days.", fontsize=25)
    plt.ylabel("Growth in percent.", fontsize=25)
    plt.title("Comparison of the growth of " + feature +" between countries.", fontsize=30)
    lgd = ax.legend(frameon=False, borderaxespad=0., bbox_to_anchor=(0.5, -0.15), fontsize=23, loc="center", prop={'size':25}, ncol=len(data_countries))
    plt.tick_params(axis="both", labelsize=25)
    ax.tick_params(axis='x', rotation=45)
    plt.savefig(subfolder + "/" + "comparison_growth_countries_of_"+feature+"_"+str(pred), bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_comparison_lognumber_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, per_capita_boolean, gmax=60):
    subfolder = "comparisons_" +"_".join(list_countries) + "/"
    if mode_data =="confinement":
        subfolder += "analysisGraphs_sinceconfinement"
    else:
        subfolder += "analysisGraphs_noconfinement"
    
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    if feature=="tc":
        feature_word = "cases"
    else:
        feature_word = "deaths"
    
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    colors = ["red", "green", "orange", "blue", "cyan", "black", "yellow", "magenta", "navy", "crimson", "olive"]
    for i, country in enumerate(list_countries):
        _, y_n, x, y = get_data_country_aux(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        y = y[:gmax]
        if per_capita_boolean:
            y = y/dic_countries_pop[country]
        plt.plot(np.arange(len(y)), y, linewidth=9, label=country.upper(), marker="o", markersize=10, c=colors[i])
    plt.xlabel("Days since confinement", fontsize=30)
    plt.ylabel("Log of the number of " + feature_word, fontsize=30)
    plt.yscale("log")
    plt.xticks(np.arange(0, gmax, step=4))
    
    title = "Number of " +feature_word+ " per capita" if per_capita_boolean else "Number of " +feature_word
    title += " since confinement using log scale." if mode_data=="confinement" else " using log scale."
    plt.title(title, fontsize=30)
    
    lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(1.0, +0.50), fontsize=23, loc=0, prop={'size':30})
    plt.tick_params(axis="both", labelsize=30)
    ax.tick_params(axis='x', rotation=45)
    filename = "comparison_countries_of_"+feature_word+"_per_capita" if per_capita_boolean else "comparison_countries_of_"+feature_word
    plt.savefig(subfolder + "/" + filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.savefig(subfolder + "/" + filename+".pdf", bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_comparison_ratios_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, gmax=60):
    subfolder = "comparisons_" +"_".join(list_countries)+"/"
    if mode_data=="confinement":
        subfolder += "analysisGraphs_sinceconfinement"
    else:
        subfolder += "analysisGraphs_noconfinement"
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    if feature=="tc":
        feature_word = "cases"
    else:
        feature_word = "deaths"
    
    ma = 4
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    for i, country in enumerate(list_countries):
        _, y_n, x, y = get_data_country_aux(list_countries[i], data, feature, mode_data, all_dates, dic_locs)
        y = y[:gmax]
        y = (movingaverage(y[1:]/y[:-1], ma)-1)*100
        plt.plot(np.arange(len(y)), y, linewidth=9, label=country.upper(), marker="o", markersize=10)
    plt.xlabel("Days since confinement", fontsize=30)
    plt.ylabel("Growth w.r.t. previous day in %" + feature_word, fontsize=30)
    plt.xticks(np.arange(0, gmax, step=4))
    if mode_data=="confinement":
        plt.title("Increase in % of the number of " +feature_word+" since confinement.", fontsize=30)
    else:
        plt.title("Increase in % of the number of " +feature_word+" between countries.", fontsize=30)
    lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(1.0, +0.70), fontsize=23, loc=0, prop={'size':30})
    plt.tick_params(axis="both", labelsize=30)
    ax.tick_params(axis='x', rotation=45)
    plt.savefig(subfolder + "/" + "comparison_ratios_of_"+feature_word, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.savefig(subfolder + "/" + "comparison_ratios_of_"+feature_word+".pdf", bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def plot_comparison_daily_all_countries(data, list_countries, feature, mode_data, all_dates, dic_locs, dic_countries_pop, normed, fit_function, gmax=60, pred=True):
    subfolder = "comparisons_" +"_".join(list_countries)+"/"
    if mode_data =="confinement":
        subfolder += "analysisGraphs_sinceconfinement"
    else:
        subfolder += "analysisGraphs_noconfinement"
    
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    
    if feature=="tc":
        feature_word = "cases"
    else:
        feature_word = "deaths"
    
    plt.clf()
    f = plt.figure(figsize=(16,16))
    ax = f.add_subplot(111)
    colors = ["red", "green", "orange", "blue", "cyan", "black", "yellow", "magenta", "navy", "crimson", "olive"]

    def get_y(country, data, feature, mode_data, all_dates, dic_locs, normed):
        x_n, y_n, x_t, y_t = get_data_country_aux(country, data, feature, mode_data, all_dates, dic_locs)
        ma = 4
        y = movingaverage(y_n, ma)
        y = y[:gmax]
        y_real = list()
        [y_real.extend([i]*int(y[i])) for i in range(len(y))]
        return y, y_real

    for i, country in enumerate(list_countries):
        y, y_real = get_y(country, data, feature, mode_data, all_dates, dic_locs, normed)
        days = np.arange(gmax)+1

        if fit_function==logcauchy or fit_function==lognormal:
            popt, _ = curve_fit(fit_function, days[:len(y)], np.log(y/np.sum(y)), method="dogbox", maxfev=5000, \
                bounds=([-np.inf, (1+np.argmax(y)), -np.inf], [np.inf, np.inf, np.inf]))
            preds = np.exp(fit_function(days, popt[0], popt[1], popt[2]))
        elif fit_function==log_normal:
            popt, _ = curve_fit(fit_function, days[:len(y)], y/np.sum(y), method="dogbox", maxfev=5000, \
                bounds=([-np.inf, np.log(1+np.argmax(y)), -np.inf], [np.inf, np.inf, np.inf]))
            preds = fit_function(days, popt[0], popt[1], popt[2])
        elif fit_function==loglog_normal:
            popt, _ = curve_fit(fit_function, days[:len(y)], np.log(y/np.sum(y)), method="dogbox", maxfev=5000, \
                bounds=([-np.inf, np.log(1+np.argmax(y)), -np.inf], [np.inf, np.inf, np.inf]))
            preds = np.exp(fit_function(days, popt[0], popt[1], popt[2]))
        else:
            popt, _ = curve_fit(fit_function, days[:len(y)], y/np.sum(y), method="dogbox", maxfev=5000, \
                bounds=([-np.inf, (1+np.argmax(y)), -np.inf], [np.inf, np.inf, np.inf]))
            preds = fit_function(days, popt[0], popt[1], popt[2])
        pred_total = np.amax(y)/preds[np.argmax(y)]
        if normed:
            y /= pred_total
        else:
            preds *= pred_total
        plt.plot(days[:len(y)], y, linestyle="solid", linewidth=4, label=country.upper(), marker="o", markersize=12, c=colors[i])
        plt.plot(days, preds, linewidth=7, linestyle="dotted", c=colors[i])
    
    plt.xlabel("Days since confinement", fontsize=30)
    plt.ylabel("Number of daily " + feature_word, fontsize=30)
    plt.xticks(np.arange(0, gmax, step=4))
    
    title = "Daily number of " +feature_word+ ", normed." if normed else "Daily number of " + feature_word
    title += " since confinement." if mode_data=="confinement" else "."
    plt.title(title, fontsize=30)
    
    lgd = ax.legend(frameon=False, ncol=1, borderaxespad=0., bbox_to_anchor=(1.0, +0.50), fontsize=23, loc=0, prop={'size':30})
    plt.tick_params(axis="both", labelsize=30)
    ax.tick_params(axis='x', rotation=45)
    funcname = str(fit_function).split(" ")[1]
    filename = "comparison_countries_of_daily_"+feature_word+"_normed_"+funcname if normed else "comparison_countries_of_daily_" + feature_word+funcname
    plt.savefig(subfolder + "/" + filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()